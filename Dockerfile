FROM registry.gitlab.ics.muni.cz:443/cloud/g2/container-registry/docker.io__nginx:latest

LABEL maintainer="MetaCentrum Cloud Team <cloud[at]ics.muni.cz>" \
      org.label-schema.schema-version="1.0.0-rc.1" \
      org.label-schema.vendor="Masaryk University, ICS" \
      org.label-schema.name="custom-openstack-exporter" \
      org.label-schema.version="$VERSION" \
      org.label-schema.build-date="$BUILD_DATE" \
      org.label-schema.build-ci-job-name="$CI_BUILD_JOB_NAME" \
      org.label-schema.build-ci-build-id="$CI_BUILD_ID" \
      org.label-schema.build-ci-host-name="$CI_BUILD_HOSTNAME" \
      org.label-schema.url="https://gitlab.ics.muni.cz/cloud/g2/welcomhehub-ostrava" \
      org.label-schema.vcs-url="https://gitlab.ics.muni.cz/cloud/cloud/g2/welcomhehub-ostrava" \
      org.label-schema.vcs-ref="$CI_COMMIT_SHA"

# copy conf file
COPY conf/nginx.conf /etc/nginx/nginx.conf

# grant the required permissions to the nginx user
RUN chown -R nginx:nginx /usr/share/nginx/html && \
    chmod -R 755 /usr/share/nginx/html && \
    chown -R nginx:nginx /var/cache/nginx && \
    chown -R nginx:nginx /var/log/nginx && \
    chown -R nginx:nginx /etc/nginx/conf.d

# access to the pid file
RUN touch /var/run/nginx.pid && \
    chown -R nginx:nginx /var/run/nginx.pid

# run as nginx user
USER 101

# copy the static generated website into the default location
COPY site /usr/share/nginx/html

EXPOSE 8080
