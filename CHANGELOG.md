# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.4.0] - 2025-01-28
### Added
- dynamic content reloader

## [1.3.6] - 2024-01-22
### Updated
- update copyright to 2024

## [1.3.5] - 2023-11-13
### Changed
- flab logo pentest cert

## [1.3.4] - 2023-11-06
### Changed
- Add Thanos Query links to monitoring page

## [1.3.3] - 2023-10-06
### Changed
- upd slack links

## [1.3.2] - 2023-08-31
### Changed
- monitoring page reorder buttons

## [1.3.1] - 2023-08-24
### Fixed
- common assets available for monitoring page

## [1.3.0] - 2023-08-24
### Added
- Monitoring links page (isolated yet)

## [1.2.0] - 2023-03-23
### Updated
- Added new graphical elements for index page
- Added mandatory text about einfra cloud and who is responsible for the service

## [1.1.1] - 2023-03-08
### Fixed
- minor update panel rewording

## [1.1.0] - 2023-03-08
### Fixed
- e-INFRA CZ acronym, other services added

## [1.0.3] - 2023-03-08
### Updated
- fix site_description (containing the link)

## [1.0.2] - 2023-03-08
### Updated
- added ToS, changed Horizon to Dashboard, update links

## [1.0.1] - 2023-03-07
### Updated
- Updated welcomepage structure

## [1.0.0] - 2023-03-06
### Added
- Initial changelog release
