---
template: overrides/home.html
title: "e-INFRA CZ Cloud"
hide:
    - nav
    - toc
---

##

<div class="mdx-spotlight">
    <figure class="mdx-spotlight__feature"> 
        <a href="https://docs.e-infra.cz/compute/openstack/" title="Built-in search"
            tabindex="-1"> 
            <img src="assets/dashboard-overview.png" alt="Cloud Overview" loading="lazy"
                width="500" height="327"> 
        </a>
        <figcaption class="md-typeset">
            <h2>What is e-INFRA CZ Cloud?</h2>
            <p> e-INFRA CZ Cloud offers a flexible computing and storage environment suitable for a wide variety of applications. These include, but are not limited to, web-based services and portals, container hosts, HPC worker nodes, CI/CD worker nodes, database hosts, and numerous complex scientific use cases.</p>
            <p>
                <a href="https://docs.e-infra.cz/compute/openstack/" aria-label="Learn more"> <span class="twemoji"></span> Learn more </a> 
            </p>
        </figcaption>
    </figure>
    <figure class="mdx-spotlight__feature"> <a href="#" title="e-INFRA CZ Cloud Partners"
            tabindex="-1"> 
            <img src="assets/cloud-partners.png" alt="e-INFRA CZ Cloud Partners" loading="lazy"
                width="500" height="327">
            </a>
        <figcaption class="md-typeset">
            <h2>e-INFRA CZ Cloud</h2>
            <p> The e-INFRA CZ Cloud is operated by MetaCentrum, CESNET department responsible for coordination of computing services, on behalf of e-INFRA CZ.
                Partners involved in the development of the e-INFRA CZ Cloud, providing computing resources and using the cloud service:</p>
                <ul>
                    <li>IT4Innovations National Supercomputing Center,</li>
                    <li>Institute of Computer Science of Masaryk University</li>
                    <li>and other partners representing national and international user communities</li>
                </ul>
            <p> 
                <a href="#" aria-label="Learn more"> <span class="twemoji"></span> Learn more </a> </p>
        </figcaption>
    </figure>
    <figure class="mdx-spotlight__feature"> <a href="https://docs.e-infra.cz/account/access/" title="Get Access"
            tabindex="-1"> 
            <img src="assets/dashboard.png" alt="OpenStack Access" loading="lazy"
                width="500" height="327"> </a>
        <figcaption class="md-typeset">
            <h2>Accessing e-INFRA CZ Cloud</h2>
            <p> e-INFRA CZ Cloud is accessible to</p>
                <ul>
                    <li>all users of the Czech e-infrastructure for science, development, and education,</li>
                    <li>selected national and international user communities.</li>
                </ul>
            <p> 
                <a href="https://docs.e-infra.cz/account/access/" aria-label="Learn more">
                    <span class="twemoji"></span> 
                    Learn more how to get access
                </a>
            </p>
        </figcaption>
    </figure>
</div>

# Quality assurance
We are happy to announce that the e-INFRA CZ Cloud passed penetration tests. Tests were conducted in September 2023 by CESNET Forensic Laboratory.

![](./assets/flab_logo-220.png#center)

# Other e-INFRA CZ Cloud Computing Services

<div class="grid cards" markdown>

-   __G1 OpenStack site in Brno__

    ---

    Another e-INFRA CZ OpenStack cloud site located in Brno. Use multi-cloud deployments to ensure reliability and availability of your deployed application. 

    [Login to Horizon Dashboard][ostack-brno]

-   __Container cloud__

    ---

    Simplify your infrastructure with container platform service. Streamline deployment, scaling and management of your research applications with Kubernetes.   

    [Learn more about our Kubernetes][k8s-product-page]   

<!-- -   __SensitiveCloud__

    ---
    Work with your sensitive data in Trusted Research Environment which is designed to process the data following ISO 27K and other standards. Tailored for the academic purposes.

    [Learn more about SensitiveCloud][sensitive-learn-more] -->

</div>

[ostack-brno]: https://cloud.muni.cz
[k8s-product-page]: https://www.cerit-sc.cz/infrastructure-services/data-processing/container-platform
[sensitive-learn-more]: https://www.cerit-sc.cz/infrastructure-services/sensitivecloud
