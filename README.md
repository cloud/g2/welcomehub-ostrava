# Welcomehub Ostrava

Welcome-hub page available at https://ostrava.openstack.cloud.e-infra.cz/


## Testing

```console
$ sudo apt install libpython3-dev
$ sudo apt install python3.8-venv
$ python3 -m venv venv
$ source venv/bin/activate
$ pip install -r requirements.txt
$ mkdocs build
$ mkdocs serve
```
